package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final ITaskRepository taskRepository, final IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId) throws EmptyIdException {
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Project removeProjectById(final String projectId) throws EmptyIdException {
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Task bindTaskByProjectId(final String projectId, final String taskId) throws AbstractException {
        if (DataUtil.isEmpty(projectId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
        final Task task = taskRepository.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskByProjectId(final String taskId) throws AbstractException {
        if (DataUtil.isEmpty(taskId)) throw new EmptyIdException();
        final Task task = taskRepository.findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

    @Override
    public void clearProjects() {
        final List<Project> projects = projectRepository.findAll();
        for (final Project project : projects) {
            String projectId = project.getId();
            taskRepository.removeAllByProjectId(projectId);
        }
        projectRepository.clear();
    }

}
