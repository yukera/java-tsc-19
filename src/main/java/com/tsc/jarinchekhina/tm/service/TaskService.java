package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name) throws EmptyNameException {
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task create(final String name, final String description) throws AbstractException {
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task findByIndex(final Integer index) throws EmptyIdException {
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findByName(final String name) throws EmptyNameException {
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findByName(name);
    }

    @Override
    public Task removeByIndex(final Integer index) throws EmptyIdException {
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) throws EmptyNameException {
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.removeByName(name);
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) throws AbstractException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) throws AbstractException {
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String id) throws AbstractException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) throws AbstractException {
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String name) throws AbstractException {
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = findByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final String id) throws AbstractException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index) throws AbstractException {
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name) throws AbstractException {
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = findByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) throws AbstractException {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) throws AbstractException {
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(final String name, final Status status) throws AbstractException {
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = findByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
