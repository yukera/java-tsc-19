package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.entity.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    Task create(String name) throws EmptyNameException;

    Task create(String name, String description) throws AbstractException;

    Task findByIndex(Integer index) throws EmptyIdException;

    Task findByName(String name) throws EmptyNameException;

    Task removeByIndex(Integer index) throws EmptyIdException;

    Task removeByName(String name) throws EmptyNameException;

    Task updateTaskById(String id, String name, String description) throws AbstractException;

    Task updateTaskByIndex(Integer index, String name, String description) throws AbstractException;

    Task startTaskById(String id) throws AbstractException;

    Task startTaskByIndex(Integer index) throws AbstractException;

    Task startTaskByName(String name) throws AbstractException;

    Task finishTaskById(String id) throws AbstractException;

    Task finishTaskByIndex(Integer index) throws AbstractException;

    Task finishTaskByName(String name) throws AbstractException;

    Task changeTaskStatusById(String id, Status status) throws AbstractException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractException;

    Task changeTaskStatusByName(String name, Status status) throws AbstractException;

}
