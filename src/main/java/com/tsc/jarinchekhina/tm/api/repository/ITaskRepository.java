package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void removeAllByProjectId(String projectId);

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllByProjectId(String projectId);

    Task findByIndex(Integer index);

    Task findByName(String name);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

}
