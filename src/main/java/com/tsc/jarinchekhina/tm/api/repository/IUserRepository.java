package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    User removeByLogin(String login);

    User findByLogin(String login);

    User findByEmail(String email);

}
