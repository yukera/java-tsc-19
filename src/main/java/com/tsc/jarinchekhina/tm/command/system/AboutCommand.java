package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "display developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Yuliya Arinchekhina");
        System.out.println("E-mail: arin-akira@mail.ru");
    }

}
