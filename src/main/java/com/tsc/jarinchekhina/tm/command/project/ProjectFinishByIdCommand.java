package com.tsc.jarinchekhina.tm.command.project;

import com.tsc.jarinchekhina.tm.command.AbstractProjectCommand;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;

public class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String description() {
        return "finish project by id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishProjectById(id);
        if (project == null) throw new ProjectNotFoundException();
    }

}
