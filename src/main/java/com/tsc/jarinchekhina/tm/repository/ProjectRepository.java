package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> projectsSorted = new ArrayList<>(entities);
        projectsSorted.sort(comparator);
        return projectsSorted;
    }

    @Override
    public Project findByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public Project findByName(final String name) {
        if (DataUtil.isEmpty(name)) return null;
        for (final Project project : entities) {
            if (project == null) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeByIndex(final Integer index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

}
