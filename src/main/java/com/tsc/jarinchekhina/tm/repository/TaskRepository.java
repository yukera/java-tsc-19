package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> tasksSorted = new ArrayList<>(entities);
        tasksSorted.sort(comparator);
        return tasksSorted;
    }

    @Override
    public Task findByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public Task findByName(final String name) {
        if (DataUtil.isEmpty(name)) return null;
        for (final Task task : entities) {
            if (task == null) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> tasksByProject = new ArrayList<>();
        for (final Task task : entities) {
            if (projectId.equals(task.getProjectId())) tasksByProject.add(task);
        }
        if (tasksByProject.size() > 0) return tasksByProject;
        else return null;
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        List<Task> entitiesNew = new ArrayList<>();
        for (final Task task : entities) {
            if (!projectId.equals(task.getProjectId())) entitiesNew.add(task);
        }
        entities = entitiesNew;
    }

}
