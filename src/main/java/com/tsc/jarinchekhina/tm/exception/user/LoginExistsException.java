package com.tsc.jarinchekhina.tm.exception.user;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException(String login) {
        super("Error! Login '" + login + "' already exists...");
    }

}
