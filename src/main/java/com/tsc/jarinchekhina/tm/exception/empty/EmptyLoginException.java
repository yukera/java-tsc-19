package com.tsc.jarinchekhina.tm.exception.empty;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty...");
    }

}
